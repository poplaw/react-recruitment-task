import {
    Avatar,
    Checkbox,
    Divider,
    Grid,
    ListItem,
    ListItemAvatar,
    ListItemSecondaryAction,
    Typography,
} from "@material-ui/core";
import React, { forwardRef } from "react";
import PropTypes from "prop-types";

const UsersListItemText = ({ children }) => {
    return (
        <Typography variant="h6" style={{ flex: 1 }} align="center">
            {children}
        </Typography>
    );
};

const ColumnDivider = () => <Divider orientation="vertical" flexItem />;

const UserListItem = (
    { style, avatarUrl, firstName, lastName, checked, onClick },
    ref
) => {
    return (
        <div style={style} ref={ref}>
            <ListItem button onClick={onClick}>
                <ListItemAvatar>
                    <Avatar src={avatarUrl}>
                        {`${firstName
                            .substring(0, 1)
                            .toUpperCase()}${lastName
                            .substring(0, 1)
                            .toUpperCase()}`}
                    </Avatar>
                </ListItemAvatar>
                <ColumnDivider />
                <Grid container>
                    <UsersListItemText>{firstName}</UsersListItemText>
                    <ColumnDivider />
                    <UsersListItemText>{lastName}</UsersListItemText>
                    <ColumnDivider />
                    <Checkbox checked={checked}></Checkbox>
                </Grid>
            </ListItem>
        </div>
    );
};

UserListItem.propTypes = {
    style: PropTypes.object,
    avatarUrl: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    checked: PropTypes.bool,
    onClick: PropTypes.func,
};

export default forwardRef(UserListItem);
