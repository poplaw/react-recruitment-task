import { AppBar, Grid, Toolbar, Typography } from "@material-ui/core";
import React from "react";
import PropTypes from "prop-types";

const Topbar = ({ title }) => {
    return (
        <AppBar position="sticky">
            <Toolbar>
                <Grid container item justify="center">
                    <Typography variant="h5">{title}</Typography>
                </Grid>
            </Toolbar>
        </AppBar>
    );
};

Topbar.propTypes = {
    title: PropTypes.string,
};

export default Topbar;
