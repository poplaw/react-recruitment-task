import { Box, Grid, InputBase, makeStyles } from "@material-ui/core";
import { Search } from "@material-ui/icons";
import React from "react";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: "white",
        padding: "3px 7px",
    },
    searchIcon: {
        margin: "7px",
    },
    input: {
        flex: 1,
    },
}));

const SearchBar = ({ value, onChange }) => {
    const classes = useStyles();

    return (
        <Box className={classes.root}>
            <Grid container alignItems="center" direction="row">
                <Search className={classes.searchIcon} />
                <InputBase
                    className={classes.input}
                    fullWidth
                    margin="none"
                    value={value}
                    onChange={onChange}
                />
            </Grid>
        </Box>
    );
};

SearchBar.propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func,
};

export default SearchBar;
