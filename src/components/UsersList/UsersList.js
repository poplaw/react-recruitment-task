import React, { useCallback, useEffect, useState } from "react";
import UserListItem from "../UserListItem/UserListItem";
import PropTypes from "prop-types";
import {
    AutoSizer,
    List as VirtualizedList,
    WindowScroller,
} from "react-virtualized";

const UsersList = ({ users, filter }) => {
    const [usersList, setUsersList] = useState([]);

    useEffect(() => {
        setUsersList(users.map((user) => ({ ...user, checked: true })));
    }, [users]);

    const userFilteringFunction = useCallback(
        (user) => {
            const searchedFields = ["first_name", "last_name"];

            for (const field of searchedFields) {
                if (user[field].toUpperCase().includes(filter.toUpperCase())) {
                    return true;
                }
            }
            return false;
        },
        [filter]
    );

    const onCheckboxClick = useCallback(
        (user) => {
            const tempUsersList = usersList.map((item) => {
                if (item.id === user.id) {
                    return {
                        ...item,
                        checked: !item.checked,
                    };
                } else {
                    return {
                        ...item,
                    };
                }
            });

            setUsersList(tempUsersList);
            console.log(tempUsersList.filter((user) => user.checked));
        },
        [usersList]
    );

    return (
        <WindowScroller>
            {({ height, onChildScroll, scrollTop }) => (
                <AutoSizer disableHeight>
                    {({ width }) => (
                        <VirtualizedList
                            autoHeight
                            rowHeight={56}
                            rowCount={
                                usersList.filter(userFilteringFunction).length
                            }
                            scrollTop={scrollTop}
                            overscanRowCount={2}
                            onScroll={onChildScroll}
                            width={width}
                            height={height}
                            rowRenderer={({ index, style }) => {
                                const user = usersList.filter(
                                    userFilteringFunction
                                )[index];

                                return (
                                    <UserListItem
                                        key={index}
                                        style={style}
                                        firstName={user.first_name}
                                        lastName={user.last_name}
                                        avatarUrl={user.avatar}
                                        checked={user.checked}
                                        onClick={() => onCheckboxClick(user)}
                                    />
                                );
                            }}
                        />
                    )}
                </AutoSizer>
            )}
        </WindowScroller>
    );
};

UsersList.propTypes = {
    users: PropTypes.array,
    filter: PropTypes.string,
};

export default UsersList;
