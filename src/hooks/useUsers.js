import { useEffect, useState } from "react";

const useUsers = () => {
    const [users, setUsers] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);

    const proxyUrl = "https://cors-anywhere.herokuapp.com/";
    const url =
        "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json";
    useEffect(() => {
        setIsError(false);
        setIsLoading(true);
        fetch(`${proxyUrl}${url}`)
            .then((data) => data.json())
            .then((data) => setUsers(data))
            .catch(() => setIsError(true))
            .finally(() => setIsLoading(false));
    }, []);
    return [users, isLoading, isError];
};

export default useUsers;
