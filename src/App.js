import { CssBaseline } from "@material-ui/core";
import { useState } from "react";
import SearchBar from "./components/SearchBar/SearchBar";
import Topbar from "./components/Topbar/Topbar";
import UsersList from "./components/UsersList";
import useUsers from "./hooks/useUsers";

const App = () => {
    const topbarTitle = "Contacts";

    const [users, isLoading, isUsers] = useUsers();
    const [filteredUsers, setFilteredUsers] = useState("");

    return (
        <>
            <CssBaseline />
            <Topbar title={topbarTitle} />
            <SearchBar
                value={filteredUsers}
                onChange={(e) => setFilteredUsers(e.target.value)}
            />
            <UsersList users={users} filter={filteredUsers} />
        </>
    );
};

export default App;
